A simple program that use RESTful Web Service to receive commands from EARTH to control a robot on MARS.
To send commands, use this:

--curl --request POST http://localhost:8080/rest/mars/MMRMMRMMRMM

If you have problems, everything is ok. This is a simulator and your robot is safe
