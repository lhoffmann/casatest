package com.casa.rest;

import org.junit.Assert;
import org.junit.Test;

import com.casa.entity.Ground;
import com.casa.entity.Robot;

public class MarsTest {

	@Test(expected = IllegalArgumentException.class)
	public void moveRobotTestWithWrongCommand() throws IllegalPosition {
		final String INIT_POS = "N";
		final String COMMAND = "MT";
		Robot robot = new Robot(0, 0, INIT_POS.charAt(0), new Ground(5, 5));
		for (int x = 0; x < COMMAND.length(); x++) {
			switch (COMMAND.charAt(x)) {
			case 'M':
				robot.move();
				break;
			default:
				throw new IllegalArgumentException("Invalid command");
			}
		}
	}

	@Test
	public void moveRobotTestToSamePlace() throws IllegalPosition {
		final String INIT_POS = "N";
		final String COMMAND = "MMMMMRMMMMMRMMMMMRMMMMMR";
		Robot robot = new Robot(0, 0, INIT_POS.charAt(0), new Ground(5, 5));
		for (int x = 0; x < COMMAND.length(); x++) {
			switch (COMMAND.charAt(x)) {
			case 'M':
				robot.move();
				break;
			case 'R':
				robot.turnRight();
				break;
			}
		}
		Assert.assertEquals("(0,0,N)", robot.toString());
	}
}
