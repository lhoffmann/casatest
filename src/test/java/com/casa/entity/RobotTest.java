package com.casa.entity;

import org.junit.Test;

import com.casa.rest.IllegalPosition;

public class RobotTest {

	@Test(expected = IllegalArgumentException.class)
	public void checkDirectionTest() throws IllegalPosition {
		final String INIT_POS = "U";
		new Robot(0, 0, INIT_POS.charAt(0), new Ground(5, 5));
	}
}
