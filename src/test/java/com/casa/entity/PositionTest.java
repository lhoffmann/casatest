package com.casa.entity;

import org.junit.Test;

import com.casa.rest.IllegalPosition;

public class PositionTest {

	@Test(expected = IllegalArgumentException.class)
	public void positionWithNullGround() throws IllegalPosition {
		new Position(0, 0, null);
	}

	@Test(expected = IllegalPosition.class)
	public void positionXGreatThanGround() throws IllegalPosition {
		Ground ground = new Ground(5, 5);
		new Position(6, 3, ground);
	}

	@Test(expected = IllegalPosition.class)
	public void positionYGreatThanGround() throws IllegalPosition {
		Ground ground = new Ground(5, 5);
		new Position(3, 6, ground);
	}

	@Test(expected = IllegalPosition.class)
	public void positionWithXNegativeVal() throws IllegalPosition {
		Ground ground = new Ground(5, 5);
		new Position(-1, 5, ground);
	}

	@Test(expected = IllegalPosition.class)
	public void positionWithYNegativeVal() throws IllegalPosition {
		Ground ground = new Ground(5, 5);
		new Position(1, -10, ground);
	}
}
