package com.casa.direction;

import org.junit.Assert;
import org.junit.Test;

import com.casa.rest.IllegalPosition;

public class EastTest {

	/**
	 * Move to left direction
	 * 
	 * @throws IllegalPosition
	 */
	@Test
	public void moveToLeftTest() throws IllegalPosition {
		Assert.assertEquals(new North().toString(), new East().turnLeft().toString());
	}

	/**
	 * Move to right direction
	 * 
	 * @throws IllegalPosition
	 */
	@Test
	public void moveToRightTest() throws IllegalPosition {
		Assert.assertEquals(new South().toString(), new East().turnRight().toString());
	}

	/**
	 * Get representation of direction
	 */
	@Test
	public void getStringPosition() {
		Assert.assertEquals("E", new East().toString());
	}
}