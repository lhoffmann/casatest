package com.casa.direction;

import org.junit.Assert;
import org.junit.Test;

import com.casa.rest.IllegalPosition;

public class NorthTest {

	/**
	 * Move to left direction
	 * 
	 * @throws IllegalPosition
	 */
	@Test
	public void moveToLeftTest() throws IllegalPosition {
		Assert.assertEquals(new West().toString(), new North().turnLeft().toString());
	}

	/**
	 * Move to right direction
	 * 
	 * @throws IllegalPosition
	 */
	@Test
	public void moveToRightTest() throws IllegalPosition {
		Assert.assertEquals(new East().toString(), new North().turnRight().toString());
	}

	/**
	 * Get representation of direction
	 */
	@Test
	public void getStringPosition() {
		Assert.assertEquals("N", new North().toString());
	}
}