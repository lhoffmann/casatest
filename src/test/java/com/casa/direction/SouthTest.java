package com.casa.direction;

import org.junit.Assert;
import org.junit.Test;

import com.casa.rest.IllegalPosition;

public class SouthTest {

	/**
	 * Move to left direction
	 * 
	 * @throws IllegalPosition
	 */
	@Test
	public void moveToLeftTest() throws IllegalPosition {
		Assert.assertEquals(new East().toString(), new South().turnLeft().toString());
	}

	/**
	 * Move to right direction
	 * 
	 * @throws IllegalPosition
	 */
	@Test
	public void moveToRightTest() throws IllegalPosition {
		Assert.assertEquals(new West().toString(), new South().turnRight().toString());
	}

	/**
	 * Get representation of direction
	 */
	@Test
	public void getStringPosition() {
		Assert.assertEquals("S", new South().toString());
	}
}